// config/config.js
module.exports = {

    'mongo' : 'localhost:27017/football',
    'serverPort': 3000, //changing this port require changing application address in google api
    'allowedEmailsDomains': ['codete.co', 'codete.com'],
    'admins': ['maciej.dudzinski@codete.co', 'karol@codete.com', 'karol@codete.co']

};