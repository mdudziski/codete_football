// server.js

// set up ======================================================================
// get all the tools we need
var config   = require('./config/config.js');
var express  = require('express');
var app      = express();
var port     = config.serverPort;
var mongoose = require('mongoose');
var passport = require('passport');
var flash    = require('connect-flash');

var morgan       = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var session      = require('express-session');



// configuration ===============================================================
mongoose.connect(config.mongo); // connect to our database

require('./config/passport')(passport); // pass passport for configuration

// set up our express application
app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser.json()); // get information from html forms
app.use(bodyParser.urlencoded({ extended: true }));

app.set('view engine', 'ejs'); // set up ejs for templating

// required for passport
app.use(session({ secret: 'ilovescotchscotchyscotchscotchNOPENOPENOPE' })); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session


app.use(function(req, res, next) {
    //return next();
    if(req.url.indexOf('/auth/google') !== 0) {
        if (req.isAuthenticated()) {
            console.log('log in');
            return next();
        } else {
            console.log('not log in');
            res.redirect('/auth/google');
        }
    } else {
        console.log('not need login');
        return next();
    }

});

app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/public/app'));



// routes ======================================================================
require('./app/routes.js')(app, passport); // load our routes and pass in our app and fully configured passport

// launch ======================================================================
app.listen(port);
console.log('The magic happens on port ' + port);
