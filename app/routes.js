var Game = require('../app/models/game');
var User = require('../app/models/user');

var config = require('../config/config.js');

module.exports = function (app, passport) {

// normal routes ===============================================================

    // show the home page (will also have our login links)
    app.get('/', function (req, res) {
        res.render('index.ejs');
    });
    app.get('/auth', function (req, res) {
        var admin = false;
        if (req.user && config.admins.indexOf(req.user.google.email) > -1) {
            admin = true;
        }
        res.send([
            {
                google: req.user.google,
                admin: admin
            }
        ]);
    });


    app.get('/user', function (req, res) {
        User.find({}, function (err, users) {
            if (err) throw err;
            res.send(users);
        });
    });

    app.get('/user/:email', function (req, res) {
        User.find({ "google.email": req.params.email}, function (err, users) {
            if (err) throw err;
            console.log(users);
            if (!users.length) {
                res.end("User doesn't exist");
            }
            res.send(users[0]);
        });
    });

    // show the home page (will also have our login links)
    app.post('/game', isAdmin, function (req, res) {

        var newGame = new Game();
        newGame.gameDate = new Date(+req.body.date);
        newGame.comment = req.body.comment;
        newGame.players = [];
        newGame.save(function (err, games) {
            if (err)
                throw err;

            res.send(games);
        });
    });

    app.get('/game', function (req, res) {
        console.log(req.user);
        Game.find({}, function (err, games) {
            if (err) throw err;

            games.forEach(function (game, gameIndex) {
                game.players.forEach(function (player, index) {
                    User.find({ "google.email": player}, function (err, user) {
                        if (err) throw err;
                        games[gameIndex].players[index] = user[0];
                    });
                });
            });
            setTimeout(function () {
                res.send(games); //TODO, learn how to join
            }, 200);
        });
    });

    app.get('/game/:id', function (req, res) {
        getGame(req.params.id, res)
    });

    var getGame = function (id, res, callback) {
        Game.find({_id: id}, function (err, games) {
            if (err) throw err;
            if (!games.length) {
                if (res) {
                    res.send('Game doesn\'t exist');
                }
                if (callback) {
                    callback(false);
                }
            }
            games[0].players.forEach(function (player, index) {
                User.find({ "google.email": player}, function (err, user) {
                    if (err) throw err;
                    games[0].players[index] = user;
                    if (index + 1 === games[0].players.length) {
                        if (res) {
                            res.send(games[0]);
                        }
                        if (callback) {
                            callback(games[0]);
                        }
                    }
                });
            });

            if (!games[0].players.length) {
                if (res) {
                    res.send(games[0]);
                }
                if (callback) {
                    callback(games[0]);
                }
            }
        });
    };

    app.put('/game/:id', isAdmin, function (req, res) {
        var newContent = {};
        req.body.comment ? newContent.comment = req.body.comment : false;
        req.body.date ? newContent.gameDate = new Date(+req.body.date) : false;
        req.body.date ? newContent.gameDate = new Date(+req.body.date) : false;
        req.body.redTeam ? newContent.redTeam = req.body.redTeam : false;
        req.body.redTeamScore ? newContent.redTeamScore = req.body.redTeamScore : false;
        req.body.blueTeam ? newContent.blueTeam = req.body.blueTeam : false;
        req.body.blueTeamScore ? newContent.blueTeamScore = req.body.blueTeamScore : false;
        req.body.locked ? newContent.locked = req.body.locked : false;

        Game.findByIdAndUpdate(req.params.id, newContent, {}, function (err, games) {
            if (err) throw err;
            res.send(games);
        })
    });

    app.delete('/game/:id', isAdmin, function (req, res) {
        Game.findByIdAndRemove(req.params.id, function (err, games) {
            if (err) throw err;
            res.send(games);
        })
    });

    app.get('/game/:id/balance', function (req, res) {
        balance(req.params.id, function() {
            getGame(req.params.id, res);
        })
    });

    var balance = function(gameId, callback) {
        var redTeam = [];
        var blueTeam = [];
        getGame(gameId, false, function (game) {
            game.players.forEach(function (player, index) {
                console.log(player[0]);
                if (index % 2) {
                    blueTeam.push(player[0].google.email);
                } else {
                    redTeam.push(player[0].google.email);
                }
            });
            console.log(redTeam);
            console.log(blueTeam);
            var newContent = {
                redTeam: redTeam,
                blueTeam: blueTeam
            };
            Game.findByIdAndUpdate(gameId, newContent, {}, function (err, games) {
                if (err) throw err;
                callback();
            })
        });
    };

    app.post('/game/:id/join/:email', function (req, res) {
        var email = req.params.email;
        if (!email) {
            res.end('No email');
            return false;
        }
        Game.find({_id: req.params.id}, function (err, games) {
            if (err) throw err;
            if (games[0].players.indexOf(email) !== -1) {
                res.end('Already joined');
                return false;
            }
            games[0].players.push(email);
            Game.findByIdAndUpdate(req.params.id, { players: games[0].players}, {}, function (err, games) {
                if (err) throw err;
                balance(req.params.id, function() {
                    getGame(req.params.id, res);
                })
            });
        });
    });

    app.post('/game/:id/leave/:email', function (req, res) {
        var email = req.params.email;
        if (req.user.google.email !== email && config.admins.indexOf(req.user.google.email) === -1) {
            res.end('No permission');
        }
        if (!email) {
            res.end('No email');
            return false;
        }
        Game.find({_id: req.params.id}, function (err, games) {
            if (err) throw err;
            games[0].players = games[0].players.filter(function (element) {
                return element !== email;
            });
            Game.findByIdAndUpdate(req.params.id, { players: games[0].players}, {}, function (err, games) {
                if (err) throw err;
                balance(req.params.id, function() {
                    getGame(req.params.id, res);
                })
            });
        });
    });


// LOGOUT ==============================
    app.get('/logout', function (req, res) {
        req.logout();
        res.redirect('/');
    });

// send to google to do the authentication
    app.get('/auth/google', passport.authenticate('google', { scope: ['profile', 'email'], prompt: 'select_account' })); //,

// the callback after google has authenticated the user
    app.get('/auth/google/callback',
        passport.authenticate('google', {
            successRedirect: '/',
            failureRedirect: '/'
        }));

    function isAdmin(req, res, next) {

        if (req.user && config.admins.indexOf(req.user.google.email) > -1)
            return next();

        res.end('No permission');
    }
};
