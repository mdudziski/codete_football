var Game = require('../app/models/game');
var User = require('../app/models/user');

var config = require('../config/config.js');

module.exports = {
    updateSkill: function () {
        Game.find({}, function (err, games) {
            if (err) throw err;

            games.forEach(function (game, gameIndex) {
                game.players.forEach(function (player, index) {
                    User.find({ "google.email": player}, function (err, user) {
                        if (err) throw err;
                        games[gameIndex].players[index] = user[0];
                    });
                });
            });
            setTimeout(function() {
                res.send(games); //TODO, learn how to join
            }, 50);
        });
    }

};
