'use strict';

describe('Filter: ratio', function () {

  // load the filter's module
  beforeEach(module('publicApp'));

  // initialize a new instance of the filter before each test
  var ratio;
  beforeEach(inject(function ($filter) {
    ratio = $filter('ratio');
  }));

  it('should return the input prefixed with "ratio filter:"', function () {
    var text = 'angularjs';
    expect(ratio(text)).toBe('ratio filter: ' + text);
  });

});
