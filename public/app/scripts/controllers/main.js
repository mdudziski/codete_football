'use strict';

/**
 * @ngdoc function
 * @name publicApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the publicApp
 */
angular.module('publicApp')
    .controller('MainCtrl', function ($scope, $rootScope, Restangular, $route) {
        $scope.gameEdit = false;
        $rootScope.page = 'home';

        $scope.getGames = function () {
            Restangular.all('game').getList()
                .then(function (games) {
                    $scope.games = games.filter(function (element) {
                        return (new Date(element.gameDate)) > new Date();
                    });
                });
        };

        $scope.getGames();

        $scope.join = function (game) {
            game.customPOST({}, "join/" + $rootScope.me.google.email, {}, {}).then(function () {
                $scope.getGames();
            });
        };

        $scope.leave = function (game) {
            game.customPOST({}, "leave/" + $rootScope.me.google.email, {}, {}).then(function () {
                $scope.getGames();
            });
        };

        $scope.alreadyJoined = function (game) {
            return game.players.some(function (player) {
                return player.google.email === $rootScope.me.google.email;
            });
        };

        $scope.remove = function (game) {
            if (confirm("Sure?")) {
                game.remove();
                $scope.games = $scope.games.filter(function (element) {
                    return element._id !== game._id;
                });
            }
        };

        $scope.startEditing = function () {
            $scope.gameEdit = true;
            $('.datetimepicker').datetimepicker({
                format: 'yyyy-mm-dd hh:ii',
                startDate: new Date()
            });
        };

        $scope.edit = function (game, date, comment) {
            console.log(game, date, comment);
            game.comment = comment;
            game.date = new Date(date).getTime();
            game.put();
            $scope.gameEdit = false;
            $scope.getGames();
        };

        $scope.throwUser = function (game, email) {
            if (confirm('Sure?')) {
                game.customPOST({}, "leave/" + email, {}, {}).then(function () {
                    $scope.getGames();
                });
            }
        };

        $scope.getRatio = function (player) {
            player.wins = player.wins || 0;
            player.loses = player.loses || 0;
            if (player.loses === 0) {
                return player.wins;
            }
            return player.wins / player.loses;
        }

    });
