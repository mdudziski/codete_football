'use strict';

/**
 * @ngdoc function
 * @name publicApp.controller:AddCtrl
 * @description
 * # AddCtrl
 * Controller of the publicApp
 */
angular.module('publicApp')
    .controller('AddCtrl', function ($scope, $rootScope, Restangular, growlNotifications) {
        $rootScope.page = 'add';
        $scope.add = function() {
            var game = Restangular.all('game');
            var date = new Date($scope.date).getTime();
            if(!date) {
                growlNotifications.add('Invalid date', 'danger');
                return false;
            }
            game.post({
                date: date,
                comment: $scope.comment
            }).then(function(err) {
                growlNotifications.add('Game added!', 'success');
                $scope.data = null;
                $scope.comment = null;
            }, function(err) {
                growlNotifications.add('Error: ' + err, 'danger');
            });
        };


    });
