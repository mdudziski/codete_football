'use strict';

/**
 * @ngdoc function
 * @name publicApp.controller:ArchiveCtrl
 * @description
 * # ArchiveCtrl
 * Controller of the publicApp
 */
angular.module('publicApp')
    .controller('ArchiveCtrl', function ($scope, $rootScope, Restangular) {
        $rootScope.page = 'archive';
        $scope.settingScore = false;

        Restangular.all('game').getList()
            .then(function(games) {
                $scope.games = games.filter(function(element) {
                    return (new Date(element.gameDate)) <= new Date();
                });
            });

        $scope.remove = function(game) {
            if(confirm("Sure?")) {
                game.remove();
                $scope.games = $scope.games.filter(function(element) {
                    return element._id !== game._id;
                });
            }
        };

        $scope.saveScore = function(game, redTeamScore, blueTeamScore) {
            if(redTeamScore >= 0 && blueTeamScore  >= 0) {
                game.redTeamScore = redTeamScore;
                game.blueTeamScore = blueTeamScore;
                game.put();
                $scope.settingScore = false;
            }
        }

        $scope.openSetScore = function() {
            $scope.settingScore = true;
        }
    });
