'use strict';

/**
 * @ngdoc service
 * @name publicApp.game
 * @description
 * # game
 * Service in the publicApp.
 */
angular.module('publicApp')
  .service('game', function game() {
    // AngularJS will instantiate a singleton by calling "new" on this function
  });
