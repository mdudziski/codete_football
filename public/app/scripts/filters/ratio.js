'use strict';

/**
 * @ngdoc filter
 * @name publicApp.filter:ratio
 * @function
 * @description
 * # ratio
 * Filter in the publicApp.
 */
angular.module('publicApp')
  .filter('ratio', function () {
    return function (input) {
      return (+input).toFixed(2);
    };
  });
