'use strict';

/**
 * @ngdoc overview
 * @name publicApp
 * @description
 * # publicApp
 *
 * Main module of the application.
 */
angular
    .module('publicApp', [
        'ngAnimate',
        'ngResource',
        'restangular',
        'ngSanitize',
        'growlNotifications',
        'ngRoute'
    ])
    .config(function ($routeProvider, RestangularProvider) {

        RestangularProvider.setRestangularFields({
            id: "_id"
        });

        $routeProvider
            .when('/', {
                templateUrl: 'views/main.html',
                controller: 'MainCtrl'
            })
            .when('/about', {
                templateUrl: 'views/about.html',
                controller: 'AboutCtrl'
            })
            .when('/add', {
                templateUrl: 'views/add.html',
                controller: 'AddCtrl'
            })
            .when('/archive', {
                templateUrl: 'views/archive.html',
                controller: 'ArchiveCtrl'
            })
            .otherwise({
                redirectTo: '/'
            });
    }).run(function ($rootScope, Restangular) {
        window.rootscope = $rootScope;
        Restangular.all('auth').getList()
            .then(function (auth) {
                $rootScope.me = auth[0];
            });

        $rootScope.getPlayerFromGameByEmail = function(email, game) {
            return game.players.filter(function(player) {
               return player.google.email === email;
            })[0];
        };

        $rootScope.balanceGame = function(game) {
          //  game.customPOST({}, "balance", {}, {});
        }
    });
