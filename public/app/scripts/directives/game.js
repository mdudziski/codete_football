'use strict';

/**
 * @ngdoc directive
 * @name publicApp.directive:game
 * @description
 * # game
 */
angular.module('publicApp')
  .directive('game', function () {
    return {
      templateUrl: '/views/game.html',
      restrict: 'E',
      link: function postLink(scope, element, attrs) {
        element.text('this is the game directive');
      }
    };
  });
